package com.traveloka.calendar.repository;

import com.traveloka.calendar.models.Employee;
import com.traveloka.calendar.models.Meeting;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
public class DataRepository {

    Map<Integer, Employee> employees;
    Map<Integer, Meeting> meetings;

    @PostConstruct
    public void init() {
        employees = new HashMap<>();
        meetings = new HashMap<>();
    }

    public void addEmployee(Employee employee) {
        employees.put(employee.getId(), employee);
    }

    public Employee getEmployee(Integer id) {
        return employees.get(id);
    }

    public void updateEmployee(Employee employee) {
        if(employees.containsKey(employee.getId())) {
            employees.put(employee.getId(), employee);
        }
    }

    public Meeting addMeeting(Meeting meeting) {
        if(meeting.getId() == null) {
            meeting.setId(meetings.size()+1);
        }
        meetings.put(meeting.getId(), meeting);
        return meeting;
    }

    public Meeting getMeeting(Integer id) {
        return meetings.get(id);
    }

    public void updateMeetings(Meeting meeting) {
        if(meetings.containsKey(meeting.getId())) {
            meetings.put(meeting.getId(), meeting);
        }
    }

}
