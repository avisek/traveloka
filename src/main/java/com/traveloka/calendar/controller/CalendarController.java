package com.traveloka.calendar.controller;

import com.traveloka.calendar.exception.ValidationException;
import com.traveloka.calendar.models.Employee;
import com.traveloka.calendar.models.Meeting;
import com.traveloka.calendar.models.MeetingRequest;
import com.traveloka.calendar.service.EmployeeService;
import com.traveloka.calendar.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

@RestController
@RequestMapping("/calendar")
public class CalendarController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private MeetingService meetingService;


    @PostMapping("/employee")
    public void addEmployee(@RequestBody Employee employee) throws ValidationException {
        employeeService.addEmployee(employee);
    }

    @GetMapping("/employee/{id}/meetings")
    public List<Meeting> getEmployeeMeetings(@PathVariable("id") Integer employeeId) {
        return employeeService.getEmployeeMeetings(employeeId);
    }

    @PostMapping("/meeting")
    public Meeting createMeeting(@RequestBody MeetingRequest meetingRequest) throws ValidationException {
        return meetingService.createMeeting(meetingRequest);
    }
}