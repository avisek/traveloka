package com.traveloka.calendar.service;

import com.traveloka.calendar.exception.ValidationException;
import com.traveloka.calendar.models.Employee;
import com.traveloka.calendar.models.Meeting;
import com.traveloka.calendar.models.MeetingRequest;
import com.traveloka.calendar.repository.DataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MeetingService {

    @Autowired
    private DataRepository repository;

    public Meeting createMeeting(MeetingRequest meetingRequest) throws ValidationException {
        try {

            validateMeeting(meetingRequest);
            Meeting meeting = new Meeting();
            meeting.setTime(meetingRequest.getTime());
            meeting.setTitle(meetingRequest.getTitle());
            meeting.setDurationInMinutes(meetingRequest.getDurationInMinutes());

            List<Employee> attendees = new ArrayList<>();
            meetingRequest.getEmployeeIds().forEach(id -> {
                Employee employee = repository.getEmployee(id);
                if(employee!=null) {
                    attendees.add(employee);
                }
            });
            meeting.setInvites(attendees);

            meeting = repository.addMeeting(meeting);

            for(Employee attendee : attendees) {
                attendee.getMeetings().add(meeting);
            }

            return meeting;

        } catch(ValidationException e) {
            throw e;
        }
    }

    private void validateMeeting(MeetingRequest meeting) throws ValidationException {
        if(meeting == null ){
            throw new ValidationException("Meeting is not defined");
        }
        if(meeting.getTime() == null || meeting.getTitle() == null || meeting.getTitle().isEmpty()) {
            throw new ValidationException("Meeting time and title not defined");
        }
    }
}
