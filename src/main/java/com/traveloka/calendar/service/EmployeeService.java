package com.traveloka.calendar.service;

import com.traveloka.calendar.exception.ValidationException;
import com.traveloka.calendar.models.Employee;
import com.traveloka.calendar.models.Meeting;
import com.traveloka.calendar.repository.DataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    DataRepository repository;

    public void addEmployee(Employee employee) throws ValidationException {
        try {

            validateEmployee(employee);
            repository.addEmployee(employee);

        } catch (ValidationException e) {
            throw e;
        }
    }

    private void validateEmployee(Employee employee) throws ValidationException {
        if(employee==null) {
            throw new ValidationException("Employee not defined");
        }
        if(employee.getName().isEmpty()) {
            throw new ValidationException("Employee name cannot be null");
        }
    }

    public List<Meeting> getEmployeeMeetings(Integer id) {
        Employee employee = getEmployee(id);
        return employee.getMeetings();
    }

    public Employee getEmployee(Integer id) {
        return repository.getEmployee(id);
    }
}
