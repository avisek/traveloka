package com.traveloka.calendar.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;

public class Meeting {

    private Integer id;
    private String title;
    private Date time;
    private Integer durationInMinutes;

    @JsonIgnore
    private List<Employee> invites;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public List<Employee> getInvites() {
        return invites;
    }

    public void setInvites(List<Employee> invites) {
        this.invites = invites;
    }

    public Integer getDurationInMinutes() {
        return durationInMinutes;
    }

    public void setDurationInMinutes(Integer durationInMinutes) {
        this.durationInMinutes = durationInMinutes;
    }
}
